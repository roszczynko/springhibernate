<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: heavy
  Date: 28.02.16
  Time: 22:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
    <h1>User Manager</h1>
    <c:url var="saveUrl" value="/updateUser?id=${user.id}" />
    <form:form modelAttribute="user" method="POST" action="${saveUrl}">
        <table>
            <tr>
                <td><form:label path="id">Id:</form:label></td>
                <td><form:input path="id" disabled="true"/></td>
            </tr>

            <tr>
                <td><form:label path="name">Name:</form:label></td>
                <td><form:input path="name"/></td>
            </tr>

            <tr>
                <td><form:label path="lastname">Lastname:</form:label></td>
                <td><form:input path="lastname"/></td>
            </tr>

            <tr>
                <td><form:label path="age">Age</form:label></td>
                <td><form:input path="age"/></td>
            </tr>

        </table>
        <input type="submit" value="Save" />
    </form:form>
</body>
</html>
