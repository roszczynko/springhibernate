<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<body>
<div>
    <h1>User Manager</h1>
    <c:if test="${!empty users}">
        <table>
            <tr>
                <th>Name</th>
                <th>Lastname</th>
                <th>Age</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>${user.name}</td>
                    <td>${user.lastname}</td>
                    <td>${user.age}</td>
                    <td><a href="/deleteUser/${user.id}">Delete</a></td>
                    <td><a href="/updateUser/${user.id}">Update</a></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
<a href="addUser">Add User</a>
</body>
</html>