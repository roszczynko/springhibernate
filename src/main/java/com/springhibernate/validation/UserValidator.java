package com.springhibernate.validation;

import com.springhibernate.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by heavy on 28.02.16.
 */
@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name", "required.name", "Name is requered.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname","lastname.requered", "Lastname is requered.");
    }
}
