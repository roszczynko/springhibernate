package com.springhibernate.repository;

import com.springhibernate.domain.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by heavy on 28.02.16.
 */

@Repository
@Transactional
public class UserRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public void addUser(User user){
        getSession().save(user);
    }

    public List<User> listAll(){
        return getSession().createQuery("from User").list();
    }

    public void removeUser(Integer id){
        User contact = (User) getSession().load(User.class, id);
        if (null != contact){
            getSession().delete(contact);
        }
    }

    public void updateUser(User user){
        User contact = (User) getSession().get(User.class, user.getId());
        contact.setName(user.getName());
        contact.setAge(user.getAge());
        contact.setLastname(user.getLastname());
    }

    public User get(Integer id) {
        return (User)getSession().get(User.class, id);
    }
}
