package com.springhibernate.controller;

import com.springhibernate.domain.User;
import com.springhibernate.repository.UserRepository;
import com.springhibernate.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by heavy on 28.02.16.
 */
@Controller
public class MyController {
    private UserRepository userRepository;
    private UserValidator userValidator;

    @Autowired
    public MyController(UserRepository userRepository, UserValidator userValidator){
        this.userValidator = userValidator;
        this.userRepository=userRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    private String getUsers(Model model){
        List<User> users = this.userRepository.listAll();
        model.addAttribute("users",users);
        return "index";
    }

    @RequestMapping(value = "addUser", method = RequestMethod.GET)
    public String addUser(Model model){
        model.addAttribute("user", new User());
        return "addUser";
    }

    @RequestMapping(value = "addUser", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user, BindingResult bindingResult){
        this.userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()){
            return "addUser";
        }
        this.userRepository.addUser(user);
        return "redirect:/";
    }

    @RequestMapping(value = "deleteUser/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer id){
        this.userRepository.removeUser(id);
        return "redirect:/";
    }

    @RequestMapping(value = "updateUser/{id}", method = RequestMethod.GET)
    public String updateUser(@PathVariable Integer id, Model model){

        model.addAttribute("user", userRepository.get(id));
        return "updateUser";
    }

    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public String updateUser(@RequestParam(value = "id", required = true) Integer id,
                             @Valid @ModelAttribute("user") User user,
                             BindingResult bindingResult, Model model){
        if (!bindingResult.hasErrors()) {
            user.setId(id);
            this.userRepository.updateUser(user);
            model.addAttribute("id", id);
            return "redirect:/";
        }
        return "updateUser/{id}";
    }
}